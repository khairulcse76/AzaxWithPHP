<?php
include_once '../vendor/autoload.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
    </head>
    <body>
        <?php
        echo "Yes working";
        ?>
        <form action="" method="post">
            <table style="margin: 0 auto; width: 600px; height: 200px;">
                <tr>
                    <td><label>Username</label><br><input type="text" name="username" id="username" style="width: 80%; height: 35px;" placeholder="Enter User Name..">
                        <label><span id="usermsg"></span></label>
                    </td>
                </tr>

            </table>
        </form>

        <script>
            $(document).ready(function () {
                $('#username').blur(function () {
                    var username = $(this).val();
                    ajax({
                        url: "check/userCheck",
                        method: "POST",
                        dataType: "text",
                        data: {username: username},
                        success: function (data) {
                            $('#usermsg').html(data);
                        }
                    });
                });

            });
        </script>
    </body>
</html>


$(document).ready(function () {
    $('#username').blur(function () {
        var username = $(this).val();
        ajax({
            url: "check/userCheck",
            method:"POST",
            dataType: "text",
            data:{ username:username },
            success: function (data) {
                $('#usermsg').html(data);
            }
        });
    });

});//ready function close;

